<?php  
//Fichero controllers/blogController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/postModel.php');
require('models/blogModel.php');
$blog=new Blog();

//Como ya he traido  mi modelo de datos, extraigo los datos
// que luego le pasare a la vista
$entradas=$blog->dimeEntradas();

//Llamo a la vista, para que pinte los datos de $entradas
require('views/base.php');
?>