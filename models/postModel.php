<?php  
//Fichero models/postModel.php

class Post{

	public $titulo;
	public $contenido;
	public $autor;
	public $fecha;

	public function __construct($t, $c, $a, $f){
		$this->titulo=$t;
		$this->contenido=$c;
		$this->autor=$a;
		$this->fecha=$f;
	}

} //Fin de la class Post
?>