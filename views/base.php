<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MVC - Bootstrap 3</title>
 
    <!-- CSS de Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    
    <section class="container">
      <?php foreach ($entradas as $entrada) { ?>
        <article>
         <header><?php echo $entrada->titulo; ?></header>
         <section><?php echo $entrada->contenido; ?></section>
         <footer>
            <?php echo $entrada->autor; ?>
           -
            <?php echo $entrada->fecha; ?>
         </footer>
        </article>
        <hr>
      <?php } ?>    
    </section>
    
    <!-- Librería jQuery requerida por los plugins de JavaScript -->
    <script src="js/jquery-3.3.1.min.js"></script>
 
    <!-- Todos los plugins JavaScript de Bootstrap -->
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>