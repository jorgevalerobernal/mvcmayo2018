<?php  


//llamamos a las librerias que hay en /vendor/
require('vendor/autoload.php');

//Vamos a pensar, a ver, a que controlador queremos llamar
//Por defecto, llamamos a blogController.php
if(isset($_GET['c'])){
  $c=$_GET['c'];
}else{
  $c='blogController.php';
}

//Llamo al controlador
require('controllers/'.$c);

//Y ya esta....
?>